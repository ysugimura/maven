

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.text.*;
import java.util.*;
import java.util.stream.*;

public class Main {

  private static final String HEAD = "<html>\r\n" + 
      "<head>\r\n" + 
      "<style>\r\n" + 
      "* {\r\n" + 
      "font-family: sans-serif;\r\n" + 
      "}\r\n" + 
      "</style>\r\n" + 
      "</head>\r\n" + 
      "<body>\r\n" + 
      "<hr>\r\n" + 
      "<table>\r\n" + 
      "<col width=\"400\">\r\n" + 
      "<col width=\"300\">\r\n" + 
      "<col width=\"80\">\r\n" + 
      "<tr>\r\n" + 
      "<th align=\"left\">Filename</th>\r\n" + 
      "<th align=\"left\">Last modified</th>\r\n" + 
      "<th align=\"right\">Size</th>\r\n" + 
      "</tr>\r\n";

  private static final String TAIL = 
      "</table>\r\n" + 
      "<hr>\r\n" + 
      "</body>\r\n" + 
      "</html>";
      
  static final String TEMPLATE = 
    "<tr><td><a href=\"%s\">%s</a></td><td>%s</td><td align=\"right\">%s</td></tr>";

  static final String INDEX_HTML = "index.html";

  static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z");
  
  static final String[]IGNORES = new String[] {
    "build.gradle",
    "dirindex",
    "bin",
    "build"
  };
  
  public static void main(String[] args) throws IOException {
    List<String>argList = Arrays.stream(args).collect(Collectors.toList());
    Path path = Paths.get(".");
    Set<String>ignores = new HashSet<>(Arrays.asList(IGNORES));
    createForFolder(path, ignores);    
  }

  /** 指定フォルダ以下のindex.htmlを作成し、フォルダがあればそれについても行う */
  static void createForFolder(Path folder, Set<String>ignores) throws IOException {
    List<Path> subFolders = new ArrayList<>();
    List<Path> files = new ArrayList<>();
    
    Files.list(folder).forEach(file -> {     
      
      // "."で開始するファイル名、"index.html"、ignoresの名前は無視する
      String filename = file.getFileName().toString();
      if (ignores.contains(filename) || 
          filename.equals(INDEX_HTML) || 
          filename.startsWith("."))
        return;
      
      
      // フォルダとファイルを分ける
      if (Files.isDirectory(file)) subFolders.add(file);
      else                        files.add(file);
    });
    
    subFolders.sort(Comparator.naturalOrder());
    files.sort(Comparator.naturalOrder());

    // このフォルダのindex.htmlを作成
    createIndexHtml(folder, subFolders, files);
    
    // サブフォルダについて処理
    for (Path subFolder: subFolders) {
      createForFolder(subFolder, ignores);
    }
  }
  
  /**
   * 指定フォルダの中にindex.htmlを作成する
   * @param parent
   * @param folders
   * @param files
   * @throws IOException
   */
  static void createIndexHtml(Path parent, List<Path>folders, List<Path>files) throws IOException {
    
    StringBuilder s = new StringBuilder();

    // ヘッド
    s.append(HEAD);

    // 上位フォルダへのリンク
    s.append(String.format(TEMPLATE,  "..", "..", getLastModified(parent), "-") + "\r\n");    
    
    // フォルダへのリンク
    for (Path folder: folders) {
      s.append(createRow(folder, false) + "\r\n");
    }
    
    // ファイルへのリンク
    for (Path file: files) {
      s.append(createRow(file, true) + "\r\n");
    }

    // テール
    s.append(TAIL);

    // index.htmlを作成する
    Path indexHtml = Paths.get(parent.toString(), INDEX_HTML);    
    Files.write(indexHtml, s.toString().getBytes("UTF-8"));
  }

  static String createRow(Path file, boolean size) throws IOException {
    String filename = sanitize(file.getFileName().toString());
    String filesize = size? getFileSize(file):"-";
    return String.format(TEMPLATE, filename, filename, getLastModified(file), filesize) + "\r\n";
  }
  
  /** ファイルサイズを文字列で取得する */
  static String getFileSize(Path path) throws IOException {
    long size = Files.size(path);    
    return String.format("%,d", size);
  }

  /** 最終変更時刻を文字列で取得する */
  static String getLastModified(Path path) throws IOException {
    FileTime time = Files.getLastModifiedTime(path);
    Date newDate = new Date(time.toMillis());
    return DATE_TIME_FORMAT.format(newDate);
  }
  
  private static String sanitize( String str ) {
    str = str.replaceAll("&", "&amp;");
    str = str.replaceAll("<", "&lt;");
    str = str.replaceAll(">", "&gt;");
    str = str.replaceAll("\"", "&quot;");
    str = str.replaceAll("'", "&＃39;");
    return str;
 }
}
